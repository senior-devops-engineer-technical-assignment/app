{{- define "ing-app.labels" }}
  labels:
    chart: {{ .Chart.Name }}
    version: {{ .Chart.Version }}
{{- end }}
{{- define "ing.app" -}}
app_name: {{ .Release.Name }}
app_version: "{{ .Values.app.version }}"
app_last_deploy_date: {{ now | htmlDate }}
app_generator: helm
{{- end -}}