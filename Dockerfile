FROM node:latest

WORKDIR /app

COPY package.json package.json
COPY index.js index.js
EXPOSE 3000
CMD [ "yarn", "start" ]