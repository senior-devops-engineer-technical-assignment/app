# Senior DevOps Engineer Technical Assignment

[![main pipeline status](https://gitlab.com/senior-devops-engineer-technical-assignment/app/badges/main/pipeline.svg)](https://gitlab.com/senior-devops-engineer-technical-assignment/app/-/commits/main)

- [Senior DevOps Engineer Technical Assignment](#senior-devops-engineer-technical-assignment)
  - [Create Infra and Deploy app using:](#create-infra-and-deploy-app-using)
  - [The app](#the-app)
  - [How to run locally](#how-to-run-locally)
    - [Prerequisites](#prerequisites)
  - [How to deploy](#how-to-deploy)
  - [Security](#security)
  - [Gitflow](#gitflow)
  - [App Roadmap](#app-roadmap)
    - [To future](#to-future)

## Create Infra and Deploy app using:
Infra -> Terraform (Kubernetes Cluster)
Cloud provider -> AWS
CI/CD platform -> Gitlab CI/CD
Code Versioning -> Gitlab

## The app
This app is a sample javascript code to validate all the pipeline architecture. It only shows a `Hello World`. However, there is a header in the html that the user can validate in which environment he/she is.
Check here the Environments -> https://gitlab.com/senior-devops-engineer-technical-assignment/app/-/environments.

## How to run locally

### Prerequisites
Have installed:
    - Node
    - Git
    - Yarn

1. Clone this repo in your machine
2. Enter in the folder
3. Run
```bash
yarn install
```
4. Your terminal will run the application locally using the port 3000, you can test using this command
```bash
curl localhost:3000
```
5. If you get an Hello World in the middle of a not organized HTML code, the app is working

## How to deploy

This app uses [HelmCharts](app-tech-assingment/templates/).
You also can check the common [Values](app-tech-assingment/values.yaml).

A simple push into develop branch will deploy a version of the app in a EKS cluster on develop namespace and will be available in the [develop environment](http://aa5c054c188914b3598cd98fd346c60d-1642100674.us-east-1.elb.amazonaws.com/develop).

The same behaviour will occur with [stage branch](http://aa5c054c188914b3598cd98fd346c60d-1642100674.us-east-1.elb.amazonaws.com/develop). Now, the [main branch](http://aa5c054c188914b3598cd98fd346c60d-1642100674.us-east-1.elb.amazonaws.com/) besides to deploy a version of this app, also will have a HPA configured and virtual service that serves all the environments segregated by path.

Here the path schema
- `/` -> app in prod environment
- `/stage` -> app in stage environment
- `/develop` -> app in develop environment

## Security
This app uses AWS authentication as Gitlab Variables to connect, avoiding any sensitive info versioned in the code.

## Gitflow
All the changes needs to be made following the gitflow:
1. Push the new code to develop branch (A pipeline will trigger and deploy your new code into a develop environment)
2. Run a merge request via git to branch stage
3. Let other developers use/test your app in stage environment (A similar pipeline will run into stage environment)
4. Request a MR from stage to main (Needs more than 1 reviewer)
5. Add the new code via MR to main
6. Approve the deployment to prod environment

## App Roadmap
v0.1 -> Run locally the application <br>
v0.2 -> Build and Deploy into the cluster manually <br>
v0.2.1 -> Build and Deploy into the cluster using Helm <br>
v0.3 -> Build pipeline to run the app into cluster based on branches <br>
v0.4 -> Build pipeline to configure HPA and VirtualService via Helm <br>
### To future
`These versions below could not be done due the time of exercise` <br>
v0.5 -> Add a SAST and DAST code analyzer <br>
v0.6 -> Add a step to lint yml files and helm analyzer <br>
v0.7 -> Generate a correct domain name and SSL Certificate for the app <br>
v0.8 -> Move the helm code to a different repository to be reused by other apps <br>
v0.9 -> Move .gitlab-ci.yml to a different repository to be reused by other apps <br>